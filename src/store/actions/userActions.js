import User from '../../models/User'

export const ADD_USER = 'ADD_USER'
export const SET_USERS = 'SET_USERS'

export const fetchUsers = () => {
    return async (dispatch) => {
        try {
            const response = await fetch('https://time-tracking-7c2c1.firebaseio.com/users.json')

            if(!response.ok){
                throw new Error('Something Went Wrong')
            }
            const resData = await response.json();
            const loadedUsers = [];

            for (const key in resData) {
                loadedUsers.unshift(new User(
                    key,
                    resData[key].name,
                    resData[key].surname,
                    resData[key].pin
                ))
            }

            dispatch({
                type: SET_USERS,
                users: loadedUsers
            })

        } catch(err) {
            throw(err)
        }
    }
}

export const createUser = (name, surname, pin) => {
    return async (dispatch) => {
        const response = await fetch('https://time-tracking-7c2c1.firebaseio.com/users.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                surname,
                pin,
            })
        })
        if (!response.ok) {
            throw new Error('Something went wrong!')
        }

        const resData = await response.json();

        dispatch({
            type: ADD_USER,
            userData: {
                id: resData.name,
                name: name,
                surname: surname,
                pin: pin,
            }
        })
    }
}