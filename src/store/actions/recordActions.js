import Record from '../../models/Record'

export const ADD_RECORD = 'ADD_RECORD'
export const UPDATE_RECORD = 'UPDATE_RECORD'
export const SET_RECORDS ='SET_RECORDS'
export const CLEAR_CACHE = 'CLEAR_CACHE'

export const fetchRecords = () => {
    return async (dispatch) => {
        try {
            const response = await fetch('https://time-tracking-7c2c1.firebaseio.com/records.json')

            if(!response.ok){
                throw new Error('somethingWentWrong')
            }
            const resData = await response.json();
            const loadedRecords = [];

            for (const key in resData) {
                loadedRecords.concat(new Record(
                    key,
                    resData[key].userId,
                    resData[key].currentDay,
                    resData[key].startedTime,
                    resData[key].finishedTime,
                    resData[key].totalWork,
                    resData[key].totalBreak,
                    resData[key].totalLunch,
                    resData[key].previousPress,
                    resData[key].lastPress,
                    resData[key].previousStatus,
                    resData[key].lastStatus
                ))
            }

            dispatch({
                type: SET_RECORDS,
                records: loadedRecords
            })

        } catch(err) {
            throw(err)
        }
    }
}

export const createRecord = (userId, currentDay, startedTime, finishedTime, totalWork, totalBreak, totalLunch, previousPress, lastPress, previousStatus, lastStatus) => {
    return async (dispatch) => {
        const response = await fetch('https://time-tracking-7c2c1.firebaseio.com/records.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId, 
                currentDay, 
                startedTime, 
                finishedTime, 
                totalWork, 
                totalBreak, 
                totalLunch,
                previousPress,
                lastPress,
                previousStatus,
                lastStatus
            })
        })
        if(!response.ok){
            throw new Error('Something went wrong!')
        }

        const resData = await response.json();

        dispatch({
            type: ADD_RECORD,
            recordData: {
                id: resData.name,
                userId: userId,
                currentDay: currentDay,
                startedTime: startedTime,
                finishedTime: finishedTime,
                totalWork: totalWork,
                totalBreak: totalBreak,
                totalLunch: totalLunch,
                previousPress: previousPress,
                lastPress: lastPress,
                previousStatus: previousStatus,
                lastStatus: lastStatus
            }
        })
    }
}

export const updateRecord = (id, finishedTime, totalWork, totalBreak, totalLunch, previousPress, lastPress, previousStatus, lastStatus) => {
    return async (dispatch) => {
        const response = await fetch(`https://time-tracking-7c2c1.firebaseio.com/records/${id}.json`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id,
                finishedTime, 
                totalWork, 
                totalBreak,
                totalLunch,
                previousPress,
                lastPress,
                previousStatus,
                lastStatus
            })
        })
        if(!response.ok){
            throw new Error('somethingWentWrong')
        }
        dispatch({
            type: UPDATE_RECORD,
            rid: id,
            recordData: {
                finishedTime: finishedTime,
                totalWork: totalWork,
                totalBreak: totalBreak,
                totalLunch: totalLunch,
                previousPress: previousPress,
                lastPress: lastPress,
                previousStatus: previousStatus,
                lastStatus: lastStatus
            }
        })
    }
}

export const clearCache = (id) => {
    return async (dispatch) => {
        await fetch(`https://time-tracking-7c2c1.firebaseio.com/records/${id}.json`, {
            method: 'DELETE'
        })
        dispatch ({type: CLEAR_CACHE, rid: id})
    }
}