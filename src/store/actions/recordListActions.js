import SingleRecord from '../../models/SingleRecord'

export const ADD_SINGLE_RECORD = 'ADD_SINGLE_RECORD'
export const SET_RECORD_LIST ='SET_RECORD_LIST'


export const fetchRecordList = () => {
    return async (dispatch) => {
        try {
            const response = await fetch('https://time-tracking-7c2c1.firebaseio.com/recordlist.json')

            if(!response.ok){
                throw new Error('Something Went Wrong')
            }
            const resData = await response.json();
            const loadedRecordList = [];

            for (const key in resData) {
                loadedRecordList.unshift(new SingleRecord(
                    key,
                    resData[key].userId,
                    resData[key].currentDay,
                    resData[key].previousPress,
                    resData[key].lastPress,
                    resData[key].previousStatus,
                    resData[key].lastStatus,
                    resData[key].isCheckedOut
                ))
            }

            dispatch({
                type: SET_RECORD_LIST,
                recordList: loadedRecordList
            })

        } catch(err) {
            throw(err)
        }
    }
}

export const createSingleRecord = (userId, currentDay, previousPress, lastPress, previousStatus, lastStatus, isCheckedOut) => {
    return async (dispatch) => {
        const response = await fetch('https://time-tracking-7c2c1.firebaseio.com/recordlist.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId, 
                currentDay, 
                previousPress,
                lastPress,
                previousStatus,
                lastStatus,
                isCheckedOut
            })
        })
        if(!response.ok){
            throw new Error('Something went wrong!')
        }

        const resData = await response.json();

        dispatch({
            type: ADD_SINGLE_RECORD,
            recordData: {
                id: resData.name,
                userId: userId,
                currentDay: currentDay,
                previousPress: previousPress,
                lastPress: lastPress,
                previousStatus: previousStatus,
                lastStatus: lastStatus,
                isCheckedOut: isCheckedOut
            }
        })
    }
}