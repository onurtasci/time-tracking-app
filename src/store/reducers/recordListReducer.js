import { ADD_SINGLE_RECORD, SET_RECORD_LIST } from '../actions/recordListActions'
import SingleRecord from '../../models/SingleRecord'

const initialState = {
    recordList: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_RECORD_LIST:
            return {
                ...state,
                recordList: action.recordList
            }

        case ADD_SINGLE_RECORD:
            const newSingleRecord = new SingleRecord(
                action.recordData.id,
                action.recordData.userId,
                action.recordData.currentDay,
                action.recordData.previousPress,
                action.recordData.lastPress,
                action.recordData.previousStatus,
                action.recordData.lastStatus,
                action.recordData.isCheckedOut
            )
            console.log('ddd: ', newSingleRecord);
            return {
                ...state,
                recordList: state.recordList.concat(newSingleRecord)
            }

        default:
            break;

    }
    return state
}
