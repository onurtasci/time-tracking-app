import { ADD_RECORD, UPDATE_RECORD, SET_RECORDS, CLEAR_CACHE } from '../actions/recordActions'
import Record from '../../models/Record'

const initialState = {
    allRecords: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_RECORDS:
            return {
                ...state,
                allRecords: action.records
            }

        case ADD_RECORD:
            const newRecord = new Record(
                action.recordData.id,
                action.recordData.userId,
                action.recordData.currentDay,
                action.recordData.startedTime,
                action.recordData.finishedTime,
                action.recordData.totalWork,
                action.recordData.totalBreak,
                action.recordData.totalLunch,
                action.recordData.previousPress,
                action.recordData.lastPress,
                action.recordData.previousStatus,
                action.recordData.lastStatus
            )
            return {
                ...state,
                allRecords: state.allRecords.concat(newRecord)
            }

        case UPDATE_RECORD:
            const allRecordsIndex = state.allRecords.findIndex(rec => rec.id === action.rid)
            const updatedRecord = new Record(
                action.rid,
                state.allRecords[allRecordsIndex].userId,
                state.allRecords[allRecordsIndex].currentDay,
                state.allRecords[allRecordsIndex].startedTime,
                action.recordData.finishedTime,
                action.recordData.totalWork,
                action.recordData.totalBreak,
                action.recordData.totalLunch,
                action.recordData.previousPress,
                action.recordData.lastPress,
                action.recordData.previousStatus,
                action.recordData.lastStatus
            )

            const updatedAllRecords = [...state.allRecords]
            updatedAllRecords[allRecordsIndex] = updatedRecord

            return {
                ...state,
                allRecords: updatedAllRecords
            }

        case CLEAR_CACHE:
            return {
                ...state,
                allRecords: state.allRecords.filter(rec => rec.id !== action.rid)
            }

        default:
            break;

    }
    return state
}
