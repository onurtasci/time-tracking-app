import { ADD_USER, SET_USERS } from '../actions/userActions'
import User from '../../models/User'

const initialState = {
    allUsers: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_USERS:
            return {
                ...state,
                allUsers: action.users
            }

        case ADD_USER:
            const newUser = new User(
                action.userData.id,
                action.userData.name,
                action.userData.surname,
                action.userData.pin
            )

            return {
                ...state,
                allUsers: state.allUsers.concat(newUser)
            }

        default:
            break;
    }
    return state
}