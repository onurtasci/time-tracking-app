import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://time-tracking-7c2c1.firebaseio.com/'
})

export default instance