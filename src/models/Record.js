class Record {
    constructor(id, userId, currentDay, startedTime, finishedTime, totalWork, totalBreak, totalLunch, previousPress, lastPress, previousStatus, lastStatus) {
        this.id = id
        this.userId = userId
        this.currentDay = currentDay
        this.startedTime = startedTime
        this.finishedTime = finishedTime
        this.totalWork = totalWork
        this.totalBreak = totalBreak
        this.totalLunch = totalLunch
        this.previousPress = previousPress
        this.lastPress = lastPress
        this.previousStatus = previousStatus
        this.lastStatus = lastStatus
    }
}

export default Record