class SingleRecord {
    constructor(id, userId, currentDay, previousPress, lastPress, previousStatus, lastStatus, isCheckedOut) {
        this.id = id
        this.userId = userId
        this.currentDay = currentDay
        this.previousPress = previousPress
        this.lastPress = lastPress
        this.previousStatus = previousStatus
        this.lastStatus = lastStatus
        this.isCheckedOut = isCheckedOut
    }
}

export default SingleRecord