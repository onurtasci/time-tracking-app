class User {
    constructor(id, name, surname, pin) {
        this.id = id
        this.name = name
        this.surname = surname
        this.pin = pin
    }
}

export default User