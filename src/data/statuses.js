export default [
    { id: 0, label: 'Offline', value: 'offline', color: '#ccc' },
    { id: 1, label: 'Available', value: 'available', color: '#008000' },
    { id: 2, label: 'Break', value: 'break', color: '#ffd700' },
    { id: 4, label: 'Lunch', value: 'lunch', color: '#fa8072' },
    { id: 5, label: 'Checked Out', value: 'checkedout', color: '#003366' }
]