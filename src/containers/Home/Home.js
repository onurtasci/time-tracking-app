import React from 'react'
import Aux from '../../hoc/Aux'
import EmployeeProfile from '../EmployeeProfile/EmployeeProfile'
import AdminProfile from '../AdminProfile/AdminProfile'
import AddUser from '../AddUser/AddUser'
import LiveTrack from '../LiveTrack/LiveTrack'
import { Route } from 'react-router-dom'

const home = props => {
    return (
        <Aux>
            <div>Home</div>
            <Route path="/" exact component={EmployeeProfile} />
            <Route path="/reports" exact component={AdminProfile} />
            <Route path="/add-user" exact component={AddUser} />
            <Route path="/live-track" exact component={LiveTrack} />
        </Aux>
    )
}

export default home