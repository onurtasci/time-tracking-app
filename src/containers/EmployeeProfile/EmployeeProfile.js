import React, { useState, useEffect, useCallback } from 'react'
import Aux from '../../hoc/Aux'
import classes from './EmployeeProfile.module.css'
import moment from 'moment/moment.js'
import InfoBox from '../../components/UI/InfoBox/InfoBox'
import axios from '../../axios-time'
import Spinner from '../../components/UI/Spinner/Spinner'
import errorHandler from '../../hoc/ErrorHandler/errorHandler'
import * as userActions from '../../store/actions/userActions'
import * as recordListActions from '../../store/actions/recordListActions'
import { useDispatch, useSelector } from 'react-redux'
import Summary from '../../components/UI/Summary/Summary'
import { secondsToHours, hoursToSeconds } from '../../utils/timeTransformer'
import statusesData from '../../data/statuses'

const Employee = () => {
    const [seconds, setSeconds] = useState(0);
    const [isActive, setIsActive] = useState(false);
    const [startedTime, setStartedTime] = useState()
    const [finishedTime, setFinishedTime] = useState()
    const [currentStatus, setCurrentStatus] = useState(0)
    const [currentPress, setCurrentPress] = useState(null)
    const [errorMessage, setErrorMessage] = useState(null)

    const [currentUser, setCurrentUser] = useState(null)
    const [userPin, setUserPin] = useState('')
    const [todaysRecords, setTodaysRecords] = useState(null)

    const [authenticated, setAuthenticated] = useState(false)
    const [loading, setLoading] = useState(false)
    const [totalWork, setTotalWork] = useState(0)
    const [totalBreak, setTotalBreak] = useState(0)
    const [totalLunch, settotalLunch] = useState(0)
    const [fetching, setFetching] = useState(false)
    const [loginTime, setLoginTime] = useState(moment().format('HH:mm:ss'))
    const [firstPress, setFirstPress] = useState(false)

    const dispatch = useDispatch()

    const loadRecords = useCallback(async () => {
        setFetching(true)
        try {
            await dispatch(userActions.fetchUsers())
            await dispatch(recordListActions.fetchRecordList())
        } catch (err) {
            console.log(err);
        }
        setFetching(false)
    }, [dispatch, setFetching])

    const allRecords = useSelector(state => state.recordList.recordList)
    const allUsers = useSelector(state => state.users.allUsers)

    useEffect(() => {
        loadRecords()
    }, [loadRecords])

    useEffect(() => {
        let interval = null;
        if (isActive && currentStatus) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds + 1);
            }, 1000);
        } else if (!isActive && seconds !== 0) {
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [isActive, seconds, currentStatus]);

    const loginHandler = () => {
        let user = allUsers.find(u => u.pin === userPin)
        if (user) {
            setCurrentUser(user)
            let uRecords = allRecords.filter(rec => rec.userId === userPin)
            if (uRecords.length > 0) {
                let todRecords = uRecords.filter(rec => rec.currentDay === moment().format('YYYY-MM-DD'))
                let lastRecord = todRecords[0]
                let firstRecord = todRecords[todRecords.length - 1]

                let tw = 0
                let tb = 0
                let tl = 0

                if (todRecords.length > 0) {
                    setTodaysRecords(todRecords)
                    setCurrentStatus(lastRecord.lastStatus)
                    setCurrentPress(lastRecord.lastPress)

                    let userWorks = uRecords.filter(rec => rec.currentDay === moment().format('YYYY-MM-DD')).filter(r => r.previousStatus === 1)

                    for (const key in userWorks) {
                        tw = tw + hoursToSeconds(userWorks[key].previousPress, userWorks[key].lastPress)
                    }
                    setTotalWork(tw)
                    let userBreaks = uRecords.filter(rec => rec.currentDay === moment().format('YYYY-MM-DD')).filter(r => r.previousStatus === 2)

                    for (const key in userBreaks) {
                        tb = tb + hoursToSeconds(userBreaks[key].previousPress, userBreaks[key].lastPress)
                    }
                    setTotalBreak(tb)
                    let userLunchs = uRecords.filter(rec => rec.currentDay === moment().format('YYYY-MM-DD')).filter(r => r.previousStatus === 4)

                    for (const key in userLunchs) {
                        tl = tl + hoursToSeconds(userLunchs[key].previousPress, userLunchs[key].lastPress)
                    }
                    settotalLunch(tl)
                    setStartedTime(firstRecord.lastPress)
                    if (lastRecord.isCheckedOut === false) {
                        switch (lastRecord.lastStatus) {
                            case 1: {
                                setTotalWork(tw + hoursToSeconds(lastRecord.lastPress, moment().format('HH:mm:ss')))
                                break
                            }
                            case 2: {
                                setTotalBreak(tb + hoursToSeconds(lastRecord.lastPress, moment().format('HH:mm:ss')))
                                break
                            }
                            case 4: {
                                settotalLunch(tl + hoursToSeconds(lastRecord.lastPress, moment().format('HH:mm:ss')))
                                break
                            }
                            default: {
                                break
                            }
                        }
                    } else {
                        setFinishedTime(lastRecord.lastPress)
                        setIsActive(false)
                    }
                    setIsActive(true)
                }
            }
            setAuthenticated(true)
            setLoginTime(moment().format('HH:mm:ss'))
        } else {
            setErrorMessage('Please enter a valid pin')
            setUserPin(null)
            return
        }
    }

    const startWorking = () => {
        if (todaysRecords) {
            setStartedTime(todaysRecords[todaysRecords.length - 1].lastPress)
        } else {
            setStartedTime(moment().format('HH:mm:ss'));
        }

        calculateTotalTime()
        setSeconds(0)
        setCurrentStatus(1)
        setCurrentPress(moment().format('HH:mm:ss'))
        setIsActive(true);
        saveRecordHandler(1, null, false)
    }
    const startBreak = () => {
        calculateTotalTime()
        setSeconds(0)
        setCurrentStatus(2)
        setCurrentPress(moment().format('HH:mm:ss'))
        saveRecordHandler(2, null, false)
    }
    const startLunch = () => {
        calculateTotalTime()
        setSeconds(0)
        setCurrentStatus(4)
        setCurrentPress(moment().format('HH:mm:ss'))
        saveRecordHandler(4, null, false)
    }
    const checkOut = () => {
        calculateTotalTime()
        setSeconds(0);
        setFinishedTime(moment().format('HH:mm:ss'))
        saveRecordHandler(5, moment().format('HH:mm:ss'), true)
        setIsActive(false);
        setCurrentStatus(5)
    }
    const calculateTotalTime = () => {
        console.log('currentPress: ', currentPress)

        switch (currentStatus) {
            case 1: {
                let time
                if (todaysRecords && !firstPress) {
                    time = totalWork + hoursToSeconds(loginTime, moment().format('HH:mm:ss'))
                    setFirstPress(true)
                } else {
                    time = totalWork + hoursToSeconds(currentPress, moment().format('HH:mm:ss'))
                }
                setTotalWork(time)
                break
            }
            case 2: {
                let time
                if (todaysRecords && !firstPress) {
                    time = totalBreak + hoursToSeconds(loginTime, moment().format('HH:mm:ss'))
                    setFirstPress(true)
                } else {
                    time = totalBreak + hoursToSeconds(currentPress, moment().format('HH:mm:ss'))
                }
                setTotalBreak(time)
                break
            }
            case 4: {
                let time
                if (todaysRecords && !firstPress) {
                    time = totalLunch + hoursToSeconds(loginTime, moment().format('HH:mm:ss'))
                    setFirstPress(true)
                } else {
                    time = totalLunch + hoursToSeconds(currentPress, moment().format('HH:mm:ss'))
                }
                settotalLunch(time)
                break
            }
            default: {
                break
            }
        }
    }
    const saveRecordHandler = async (nextStatus, coTime, isCout) => {
        setLoading(true)
        await dispatch(recordListActions.createSingleRecord(userPin, moment().format('YYYY-MM-DD'), currentPress, moment().format('HH:mm:ss'), currentStatus, nextStatus, isCout))
        setLoading(false)
    }

    if (fetching) {
        return <Spinner padding={300} />
    }

    let summary = <Summary
        title='Summary'
        startedTime={startedTime}
        finishedTime={finishedTime}
        totalWork={secondsToHours(totalWork)}
        totalBreak={secondsToHours(totalBreak)}
        totalLunch={secondsToHours(totalLunch)}
        dangerWork={totalWork < 27000}
        dangerBreak={totalBreak + totalLunch > 2700}
        dangerLunch={totalBreak + totalLunch > 2700}
    />

    if (loading) {
        summary = <Spinner />
    }

    const pinChangeHandler = (event) => {
        setUserPin(event.target.value)
        setErrorMessage(null)
    }

    if (!authenticated) {
        return (
            <div className={classes.Form}>
                <label>Enter Your Pin: </label>
                <input style={{ border: errorMessage ? '2px solid #ff0033' : null }} type="password" value={userPin} onChange={pinChangeHandler} maxLength={4} />
                <button onClick={loginHandler} type="submit" >Login</button>
                {errorMessage &&
                    <h4 style={{ color: '#ff0033' }} >{errorMessage}</h4>
                }
            </div>
        )
    }

    return (
        <Aux>
            <div className={classes.Profile}>
                <div className={classes.Title} >
                    <h2>{currentUser.name + ' ' + currentUser.surname}</h2>
                    <span>
                        <h4>Current Status: </h4>
                        <h4 style={{ color: '#5ac18e', fontWeight: 'bold', marginLeft: '10px' }} >({statusesData.find(s => s.id === currentStatus).label})</h4>
                        {currentStatus !== 5 && currentPress && <p>{secondsToHours(hoursToSeconds(currentPress, moment().format('HH:mm:ss')))}</p>}
                    </span>
                </div>

                {finishedTime && summary}

                <div className={classes.Grid} >
                    {!finishedTime &&
                        <InfoBox
                            title={currentStatus === 0 ? todaysRecords ? 'Continue Session' : 'Start Work' : currentStatus === 1 ? 'Available' : 'Back to Work'}
                            content={currentStatus === 1 ? secondsToHours(totalWork + seconds) : secondsToHours(totalWork)}
                            onClick={startWorking}
                            active={currentStatus === 1}
                            buttonDisabled={currentStatus === 1 ? true : false}
                        />
                    }
                    {currentStatus !== 0 && !finishedTime &&
                        <InfoBox
                            title='Break'
                            content={currentStatus === 2 ? secondsToHours(totalBreak + seconds) : secondsToHours(totalBreak)}
                            onClick={startBreak}
                            active={currentStatus === 2}
                            buttonDisabled={currentStatus === 2 || currentStatus === 0 ? true : false}
                        />
                    }
                    {currentStatus !== 0 && !finishedTime &&
                        <InfoBox
                            title='Lunch'
                            content={currentStatus === 4 ? secondsToHours(totalLunch + seconds) : secondsToHours(totalLunch)}
                            onClick={startLunch}
                            active={currentStatus === 4}
                            buttonDisabled={currentStatus === 4 || currentStatus === 0 ? true : false}
                        />
                    }
                </div>
                {currentStatus !== 5 &&
                    <span className={classes.Start}>
                        {currentStatus !== 0 &&
                            <div>
                                <p>Check In Time: </p>
                                <h4>{startedTime}</h4>
                            </div>
                        }
                        {!finishedTime && currentStatus !== 0 &&
                            <button onClick={checkOut}>Check Out</button>
                        }
                        {finishedTime &&
                            <div>
                                <p>Check Out Time: </p>
                                <h4>{finishedTime}</h4>
                            </div>
                        }
                    </span>
                }
                <span className={classes.Start}>
                    <div>
                        <p>Expected Work: </p>
                        <h4>07:15:00</h4>
                    </div>
                    <div>
                        <p>Expected Lunch + Break: </p>
                        <h4>00:45:00</h4>
                    </div>
                </span>

            </div>
        </Aux>
    )
}

export default errorHandler(Employee, axios)