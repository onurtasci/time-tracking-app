import React, { useState, useEffect, useCallback } from 'react'
import moment from 'moment/moment.js'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as userActions from '../../store/actions/userActions'
import { useDispatch, useSelector } from 'react-redux'
import { secondsToHours, hoursToSeconds } from '../../utils/timeTransformer'
import LiveBox from '../../components/UI/LiveBox/LiveBox'
import statusesData from '../../data/statuses'
import * as firebase from 'firebase'
import SingleRecord from '../../models/SingleRecord'
import { reduceArray } from '../../utils/arrayArranger'
import classes from './LiveTrack.module.css'

const LiveTrack = props => {
    const [fetching, setFetching] = useState(false)
    const [seconds, setSeconds] = useState(0);
    const [loadedRecords, setLoadedRecords] = useState([])
    const [offlineUsers, setOfflineUsers] = useState([])

    const getData = useCallback(async () => {
        setFetching(true)
        await firebase.database().ref('recordlist/').on('value', (snapshot) => {
            const resData = snapshot.val()
            const loadedRecords = []
            for (const key in resData) {
                loadedRecords.push(new SingleRecord(
                    key,
                    resData[key].userId,
                    resData[key].currentDay,
                    resData[key].previousPress,
                    resData[key].lastPress,
                    resData[key].previousStatus,
                    resData[key].lastStatus,
                    resData[key].isCheckedOut
                ))
            }
            let todRecs = loadedRecords.filter(rec => rec.currentDay === moment().format('YYYY-MM-DD'))
            let modifiedRecords = reduceArray(todRecs, 'userId')
            setLoadedRecords(modifiedRecords)

            let offlineUsers = allUsers
            for (const key in modifiedRecords) {
                offlineUsers = offlineUsers.filter(u => u.pin !== modifiedRecords[key].userId)
            }
            setOfflineUsers(offlineUsers)
            setFetching(false)
        })
    }, [setFetching, setLoadedRecords, setOfflineUsers])

    useEffect(() => {
        getData()
    }, [getData])

    useEffect(() => {
        let interval = null;
        interval = setInterval(() => {
            setSeconds(seconds => seconds + 1);
        }, 1000);

        return () => clearInterval(interval);
    }, [seconds]);

    const dispatch = useDispatch()

    const loadUsers = useCallback(async () => {
        setFetching(true)
        try {
            await dispatch(userActions.fetchUsers())
        } catch (err) {
            console.log(err)
        }
        setFetching(false)
    }, [dispatch, setFetching])

    const allUsers = useSelector(state => state.users.allUsers)

    useEffect(() => {
        loadUsers()
    }, [loadUsers])

    if (fetching) {
        return (
            <Spinner padding={300} />
        )
    }

    // if (loadedRecords.length < 1) {
    //     return (
    //         allUsers.map(u => (
    //             <LiveBox
    //                 key={u.id}
    //                 title={u.name + ' ' + u.surname}
    //                 content='No Record'
    //                 status='Offline'
    //                 statusColor={statusesData.find(s => s.id === 0).color}
    //             />
    //         ))

    //     )
    // }

    console.log('loaded: ', loadedRecords)

    return (
        <div className={classes.Screen} >
            {loadedRecords.map(rec => (
                <LiveBox
                    key={rec.id}
                    title={allUsers.find(u => u.pin === rec.userId).name + ' ' + allUsers.find(u => u.pin === rec.userId).surname} //{rec.userId}
                    content={rec.lastStatus === 5 ? rec.lastPress : secondsToHours(hoursToSeconds(rec.lastPress, moment().format('HH:mm:ss')))}
                    status={statusesData.find(s => s.id === rec.lastStatus).label}
                    statusColor={statusesData.find(s => s.id === rec.lastStatus).color}
                    preWord={rec.lastStatus === 5 ? 'at ' : 'for '}
                />
            ))}

            {offlineUsers.map(u => (
                <LiveBox
                    key={u.id}
                    title={u.name + ' ' + u.surname}
                    content='No Record'
                    status='Offline'
                    statusColor={statusesData.find(s => s.id === 0).color}
                />
            ))
            }

            {loadedRecords.length < 1 && offlineUsers.length < 1 &&
                allUsers.map(u => (
                    <LiveBox
                        key={u.id}
                        title={u.name + ' ' + u.surname}
                        content='No Record'
                        status='Offline'
                        statusColor={statusesData.find(s => s.id === 0).color}
                    />
                ))
            }
        </div>
    )
}

export default LiveTrack