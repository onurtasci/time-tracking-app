import React, { useState, useEffect, useCallback } from 'react'
import classes from './AdminProfile.module.css'
import * as userActions from '../../store/actions/userActions'
import * as recordListActions from '../../store/actions/recordListActions'
import { useDispatch, useSelector } from 'react-redux'
import Spinner from '../../components/UI/Spinner/Spinner'
import ReportBox from '../../components/UI/ReportBox/ReportBox'
import Select from 'react-select'
import { reduceArray } from '../../utils/arrayArranger'
import { hoursToSeconds, secondsToHours } from '../../utils/timeTransformer'
import statusData from '../../data/statuses'

const AdminProfile = props => {
    const [fetching, setFetching] = useState(false)
    const [selectedDay, setSelectedDay] = useState(null)
    const [selectedUser, setSelectedUser] = useState(null)
    const [startedTime, setStartedTime] = useState(null)
    const [finishedTime, setFinishedTime] = useState(null)
    const [userTodaysRecords, setUserTodaysRecords] = useState(null)
    const [selectedUserRecords, setSelectedUserRecords] = useState(null)

    const [workTime, setWorkTime] = useState(null)
    const [breakTime, setBreakTime] = useState(null)
    const [lunchTime, setLunchTime] = useState(null)

    const dispatch = useDispatch()

    const loadRecords = useCallback(async () => {
        setFetching(true)
        try {
            await dispatch(userActions.fetchUsers())
            await dispatch(recordListActions.fetchRecordList())
        } catch (err) {
            console.log(err);
        }
        setFetching(false)
    }, [dispatch, setFetching])

    useEffect(() => {
        loadRecords()
    }, [loadRecords])

    const allRecords = useSelector(state => state.recordList.recordList)
    const allUsers = useSelector(state => state.users.allUsers)

    let userListForSelect = []
    let recordListForSelect = []
    if (allUsers) {
        for (const key in allUsers) {
            userListForSelect.push({ label: allUsers[key].name + ' ' + allUsers[key].surname, value: allUsers[key].pin })
        }
    }
    if (selectedUserRecords) {
        for (const key in reduceArray(selectedUserRecords, 'currentDay')) {
            recordListForSelect.push({ label: reduceArray(selectedUserRecords, 'currentDay')[key].currentDay, value: reduceArray(selectedUserRecords, 'currentDay')[key].currentDay })
        }
    }

    if (fetching) {
        return <Spinner padding={300} />
    }

    const changeUser = (value) => {
        setSelectedUser(allUsers.find(u => u.pin === value.value))
        setSelectedUserRecords(allRecords.filter(r => r.userId === value.value))
        setUserTodaysRecords(null)
        setSelectedDay(null)
    }
    const changeDate = (event) => {
        setSelectedDay(event.value)
        setUserTodaysRecords(selectedUserRecords.filter(r => r.currentDay === event.value).reverse())
        let todaysRec = selectedUserRecords.filter(r => r.currentDay === event.value)

        let userWorks = selectedUserRecords.filter(r => r.currentDay === event.value).filter(r => r.previousStatus === 1)
        let tw = 0
        for (const key in userWorks) {
            tw = tw + hoursToSeconds(userWorks[key].previousPress, userWorks[key].lastPress)
        }
        setWorkTime(tw)

        let userBreaks = selectedUserRecords.filter(r => r.currentDay === event.value).filter(r => r.previousStatus === 2)
        let tb = 0
        for (const key in userBreaks) {
            tb = tb + hoursToSeconds(userBreaks[key].previousPress, userBreaks[key].lastPress)
        }
        setBreakTime(tb)

        let userLunchs = selectedUserRecords.filter(r => r.currentDay === event.value).filter(r => r.previousStatus === 4)
        let tl = 0
        for (const key in userLunchs) {
            tl = tl + hoursToSeconds(userLunchs[key].previousPress, userLunchs[key].lastPress)
        }
        setLunchTime(tl)

        let lastRecord = todaysRec[0]
        let firstRecord = todaysRec[todaysRec.length - 1]
        setStartedTime(firstRecord.lastPress)
        if (lastRecord.isCheckedOut === false) {
            switch (lastRecord.lastStatus) {
                case 1: {
                    setWorkTime('Current')
                    break
                }
                case 2: {
                    setBreakTime('Current')
                    break
                }
                case 4: {
                    setLunchTime('Current')
                    break
                }
                default: {
                    break
                }
            }
            setFinishedTime('----')
        } else {
            setFinishedTime(lastRecord.lastPress)
        }
    }

    return (
        <div className={classes.Form} style={{ justifyContent: 'space-between' }} >
            <div className={classes.Column} style={{paddingRight: 30}}>
                <div className={classes.SelecCon}>
                    <label className={classes.Label} >Select the User: </label>
                    <Select className={classes.Select} onChange={changeUser} options={userListForSelect} />
                </div>

                {selectedUserRecords && selectedUserRecords.length > 0 &&
                    <div>
                        <label className={classes.Label} >Select the Day: </label>
                        <Select className={classes.Select} onChange={changeDate} options={recordListForSelect} />
                    </div>
                }

                {selectedUserRecords && selectedUserRecords.length <= 0 &&
                    <h4 className={classes.NoRecord} >No records found!</h4>
                }
            </div>


            {userTodaysRecords && selectedUserRecords.length > 0 &&
                <div className={classes.Column} style={{ paddingTop: 20}}>
                    <h4 style={{ color: '#63615a' }}>Timeline Report</h4>
                    {userTodaysRecords.map(rec => (
                        <div style={{ width: '300px' }} >
                            {rec.previousPress &&
                                <div style={{ flexDirection: 'row', display: 'flex', fontWeight: 'bold' }} >
                                    <p style={{ color: '#808080', marginRight: '10px' }} >{rec.previousPress} - {rec.lastPress} : </p>
                                    <p style={{ color: statusData.find(s => s.id === rec.previousStatus).color }} >{statusData.find(s => s.id === rec.previousStatus).label}</p>
                                </div>
                            }

                        </div>
                    ))}
                    {userTodaysRecords[userTodaysRecords.length - 1].isCheckedOut === false &&
                        <div style={{ flexDirection: 'row', display: 'flex', fontWeight: 'bold' }} >
                            <p style={{ color: '#808080', marginRight: '10px' }} >{userTodaysRecords[userTodaysRecords.length - 1].lastPress} - Current  : </p>
                            <p style={{ color: statusData.find(s => s.id === userTodaysRecords[userTodaysRecords.length - 1].lastStatus).color }} >{statusData.find(s => s.id === userTodaysRecords[userTodaysRecords.length - 1].lastStatus).label}</p>
                        </div>
                    }
                </div>
            }


            {userTodaysRecords && selectedUserRecords.length > 0 &&
                <div className={classes.Column}>
                    <ReportBox
                        name={selectedUser.name + ' ' + selectedUser.surname}
                        title={selectedDay}
                        startedTime={startedTime}
                        finishedTime={finishedTime}
                        totalWork= {workTime === 'Current' ? 'Current' : secondsToHours(workTime)}
                        totalBreak={breakTime === 'Current' ? 'Current' : secondsToHours(breakTime)}
                        totalLunch={lunchTime === 'Current' ? 'Current' : secondsToHours(lunchTime)}
                        dangerWork={workTime < 27000} currentWork={workTime === 'Current'}
                        dangerBreak={breakTime + lunchTime > 2700} currentBreak={breakTime === 'Current'}
                        dangerLunch={breakTime + lunchTime > 2700} currentLunch={lunchTime === 'Current'}
                    />
                </div>
            }
        </div>
    )
}

export default AdminProfile