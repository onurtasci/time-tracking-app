import React, { useState, useCallback, useEffect } from 'react'
import classes from './AddUser.module.css'
import Spinner from '../../components/UI/Spinner/Spinner';
import * as userActions from '../../store/actions/userActions'
import { useDispatch, useSelector } from 'react-redux'

const AddUser = props => {
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [pin, setPin] = useState('')
    const [loading, setLoading] = useState(false)
    const [created, setCreated] = useState(false)
    const [errorMessage, setErrorMessage] = useState(null)

    const dispatch = useDispatch()

    const loadRecords = useCallback(async () => {
        setLoading(true)
        try {
            await dispatch(userActions.fetchUsers())
        } catch (err) {
            console.log(err);
        }
        setLoading(false)
    }, [dispatch, setLoading])

    const allUsers = useSelector(state => state.users.allUsers)

    useEffect(() => {
        loadRecords()
    }, [loadRecords])

    const nameChangeHandler = (event) => {
        setName(event.target.value)
        setErrorMessage(null)
    }
    const surnameChangeHandler = (event) => {
        setSurname(event.target.value)
        setErrorMessage(null)
    }
    const pinChangeHandler = (event) => {
        setPin(event.target.value)
        setErrorMessage(null)
    }

    const submitHandler = async (event) => {
        if (allUsers.find(u => u.pin === pin)) {
            setErrorMessage('There is an existed user with the pin you entered. Please set a different pin number')
            return
        }
        if (pin.length < 4) {
            setErrorMessage('Pin must be 4 characters')
            return
        }
        if (surname==='' || name==='') {
            setErrorMessage('You need to enter a valid name and surname')
            return
        }

        setLoading(true)
        await dispatch(userActions.createUser(name, surname, pin))
        event.preventDefault();
        setLoading(false)
        setCreated(true)
    }

    if (created) {
        return <h4 className={classes.Success} >Employee account is created succesfully!</h4>
    }

    if (loading) {
        return <Spinner padding={300} />
    }

    return (
        <div className={classes.Main}>
            <div className={classes.Form} onSubmit={submitHandler}>
                <div>
                    <label>Name: </label>
                    <input style={{border: errorMessage && name.length<=0 ? '2px solid #ff0033' : null}} type="text" value={name} onChange={nameChangeHandler} />
                </div>

                <div>
                    <label>Surname: </label>
                    <input style={{border: errorMessage && surname.length<=0 ? '2px solid #ff0033' : null}} type="text" value={surname} onChange={surnameChangeHandler} />
                </div>

                <div>
                    <label>Pin: </label>
                    <input style={{border: errorMessage && pin.length<4 ? '2px solid #ff0033' : null}} type="text" value={pin} onChange={pinChangeHandler} maxLength={4} />
                </div>

                <button onClick={submitHandler}>Add User</button>

                {errorMessage &&
                    <h4 style={{color: '#ff0033'}} >{errorMessage}</h4>
                }
            </div>
        </div>
    );
}

export default AddUser