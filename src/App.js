import React from 'react';
import Layout from './components/Layout/Layout';
import Home from './containers/Home/Home'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import recordReducer from './store/reducers/recordReducer'
import userReducer from './store/reducers/userReducer'
import recordListReducer from './store/reducers/recordListReducer'
import * as firebase from 'firebase'
import ApiKeys from './Api/apiKeys'

const rootReducer = combineReducers({
  records: recordReducer,
  users: userReducer,
  recordList: recordListReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

function App() {

  if (!firebase.apps.length) { firebase.initializeApp(ApiKeys.FirebaseConfig); }

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Layout>
          <Home/>
        </Layout>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
