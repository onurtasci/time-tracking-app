import React from 'react'
import Logo from '../../assets/images/time-tracking-logo.png'
import classes from './Logo.module.css'

const logo = props => (
    <div className={classes.Logo}>
        <img src={Logo} alt={'TimeTrackingLogo'} />
    </div>
)

export default logo