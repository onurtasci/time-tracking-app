import React from 'react'
import classes from './Summary.module.css'

const Summary = props => {
    return (
        <article className={classes.Summary} >
            {props.name && <h2>{props.name}</h2>}
            <h3>{props.title}</h3>
            <hr />
            <div>
                <p>Check In at  </p>
                <div>{props.startedTime}</div>
            </div>
            <div>
                <p>Check Out at </p>
                <div>{props.finishedTime}</div>
            </div>
            <hr />
            <div>
                <p>Total Work: </p>
                <div style={{color: props.dangerWork ? '#ff4040' : 'white'}} >{props.totalWork}</div>
            </div>
            <div>
                <p>Total Break: </p>
                <div style={{color: props.dangerBreak ? '#ff4040' : 'white'}} >{props.totalBreak}</div>
            </div>
            <div>
                <p>Total Lunch: </p>
                <div style={{color: props.dangerLunch ? '#ff4040' : 'white'}} >{props.totalLunch}</div>
            </div>
            {props.totalDay && <hr />}
            {props.totalDay &&
                <div>
                    <p>Total: </p>
                    <div>{props.totalDay}</div>
                </div>}
        </article>
    )
}

export default Summary