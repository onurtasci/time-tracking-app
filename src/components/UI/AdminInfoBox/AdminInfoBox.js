import React from 'react'
import classes from './AdminInfoBox.module.css'
import { useDispatch, useSelector } from 'react-redux'

const AdminInfoBox = props => {
    const selectedUser = useSelector(state => state.users.allUsers).find(user => user.userId === props.pin)
    return (
        <button
            className={!props.active ? classes.Card : [classes.Card, classes.Card_Active].join(' ')}
            onClick={props.onClick}
            disabled={props.buttonDisabled}
        >
            <h3>{props.title}</h3>
            <h2>{props.content}</h2>
        </button>
    )
}

export default AdminInfoBox