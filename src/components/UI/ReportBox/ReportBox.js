import React from 'react'
import classes from './ReportBox.module.css'

const ReportBox = props => {
    return (
        <article className={classes.Summary} >
            {props.name && <h2>{props.name}</h2>}
            <h3>{props.title}</h3>
            <hr style={{color: 'red'}} />
            <div>
                <p>Checked In at  </p>
                <h4 >{props.startedTime}</h4>
            </div>
            <div>
                <p>Checked Out at </p>
                <h4>{props.finishedTime}</h4>
            </div>
            <hr />
            <div>
                <p>Total Work: </p>
                <h4 style={{color: props.dangerWork ? '#ff4040' : props.currentWork ? '#daa520' : '#63615a'}} >{props.totalWork}</h4>
            </div>
            <div>
                <p>Total Break: </p>
                <h4 style={{color: props.dangerBreak ? '#ff4040' : props.currentBreak ? '#daa520' : '#63615a'}} >{props.totalBreak}</h4>
            </div>
            <div>
                <p>Total Lunch: </p>
                <h4 style={{color: props.dangerLunch ? '#ff4040' : props.currentLunch ? '#daa520' : '#63615a'}} >{props.totalLunch}</h4>
            </div>
            {props.totalDay && <hr />}
            {props.totalDay &&
                <div>
                    <p>Total: </p>
                    <h4>{props.totalDay}</h4>
                </div>}
        </article>
    )
}

export default ReportBox