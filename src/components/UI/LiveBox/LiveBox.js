import React from 'react'
import classes from './LiveBox.module.css'

const LiveBox = props => {
    return (
        <button
            className={!props.active ? classes.Card : [classes.Card, classes.Card_Active].join(' ')}
            onClick={props.onClick}
            disabled={props.buttonDisabled}
        >
            <h3>{props.title}</h3>
            <div className={classes.Status} >
                <h5>Status:</h5>
                <h4 style={{ color: props.statusColor, marginLeft: '10px' }} >{props.status}</h4>
                <h5 style={{ color: '#808080' }} >{props.preWord}</h5>
            </div>
                <h2 style={{ color: props.statusColor }} >{props.content}</h2>
        </button>
    )
}

export default LiveBox