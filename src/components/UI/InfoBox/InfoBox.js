import React from 'react'
import classes from './InfoBox.module.css'

const InfoBox = props => {
    return (
        <button
            className={!props.active ? classes.Card : [classes.Card, classes.Card_Active].join(' ')}
            onClick={props.onClick}
            disabled={props.buttonDisabled}
        >
            <h3>{props.title}</h3>
            <h2>{props.content}</h2>
        </button>
    )
}

export default InfoBox