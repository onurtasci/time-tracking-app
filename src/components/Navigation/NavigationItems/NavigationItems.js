import React from 'react'
import classes from './NavigationItems.module.css'
import NavigationItem from './NavigationItem/NavigationItem'

const NavigationItems = props => (
    <ul className={classes.NavigationItems} >
        <NavigationItem link='/' >Employee</NavigationItem>
        <NavigationItem link='/reports' active >Reports</NavigationItem>
        <NavigationItem link='/add-user' >Add User</NavigationItem>
        <NavigationItem link='/live-track' >Live Track</NavigationItem>
    </ul>
)

export default NavigationItems