
**React** Time Tracking
-----------------------------------


Installation:
    
    $ npm install

----------

Run the project:

    $ npm start

It should be running on http://localhost:3000

----------

Usage:

This is a single page React app. There are 4 tabs that can be previewed by any user without authentication.

On Emplooyee tab, employees can be login with their pin codes and change their status. (You can signin with the pin numbers '1111', '2222', '3333', '4444' or you can create a new user on Add-User page with an unique pin number.)

On Reports tab, employees time records can be listed date by date. Also you can preview starting and finishing times of short breaks or lunches.

On Add-User tab, new employees can be created with an unique pin.

On Live-Track tab, employees' current statuses can be previewed lively. Also you can see how long passed after the emplooye changed his/her status.